const axios = require('axios');
const express = require('express');
const app = express();

const db = require('../database/app');
const Merchant = db.merchants;
const customerDetails = db.customerdetails;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/api/getAllCustomer', async (req, res) => {
    try {
        const { shop } = req.query;
        const result = await Merchant.findOne();
        const accessToken = result.dataValues.AccessToken;
        let url = `https://${shop}.myshopify.com/admin/api/2021-10/customers.json?limit=5`;
        const config = {
            headers: {
                'Content-Type': 'application/json',
                'X-Shopify-Access-Token': accessToken,
            },
        };
        do {
            url = await apiCall(url, config);
        } while (url)
        console.log("url is undefined");
    } catch (err) {
        console.log(err);
        res.json({ customer: err });
    }
});

const parseNextUrlFromLink = (url) => {
    let previous = 'rel="previous", <';
    let startingposition = url.search(previous);
    let next = '>; rel="next"';
    let endingposition = url.search(next);
    if (startingposition != -1 && endingposition != -1)//link is present between these two position
    {
        startingposition = startingposition + previous.length;
        const newurl = url.substring(startingposition, endingposition);
        return newurl;
    }
    else if (startingposition != -1 && endingposition == -1)//only previous link is present
    {
        const newurl = '';
        return newurl;
    }
    else if (startingposition == -1 && endingposition != -1)//only next link is present
    {
        const newurl = url.substr(1, endingposition - 1);
        return newurl;
    }
}

const apiCall = async (Url, config) => {
    try {
        let response = await axios.get(Url, config);

        let totalcustomers = response.data.customers.length;
        for (let i = 0; i < totalcustomers; i++) {
            let dataSaveInTable = response.data.customers[i];
            let customerObject = {
                CustomerId: dataSaveInTable.id,
                Email: dataSaveInTable.email,
                OrderCount: dataSaveInTable.orders_count,
                LastOrderId: dataSaveInTable.last_order_id,
                Phone: dataSaveInTable.phone,
                FirstName: dataSaveInTable.first_name,
                LastName: dataSaveInTable.last_name
            };
            let data = await customerDetails.create(customerObject)
            console.log(`${i + 1} Data inserted`);
        }

        console.log(response.headers.link);
        let url = parseNextUrlFromLink(response.headers.link);
        return url;

    } catch (error) {
        console.log(error);
    }
}

app.get('/api/getCustomerDetails/:perPage/:pageNo', async (req, res) => {
    let perPage = Number(req.params.perPage);
    let pageNo = Number(req.params.pageNo);
    let offset = (pageNo - 1) * perPage;

    let customerDataLength = await customerDetails.count();

    if (offset < customerDataLength) {
        let data = await customerDetails.findAll({ offset: offset, limit: perPage });
        // console.log(data);
        res.send({
            "per_page": perPage,
            "status_code": 200,
            "count": customerDataLength,
            "orders": data,
            "page": pageNo
        })
    } else { 
        console.log("Error");
        res.send({ status: 404, error: "resource not found" });
    }
})

app.listen(5000, () => console.log('Application listening on port 5000!'));